'use strict';

/**
 * @ngdoc overview
 * @name chessGameApp
 * @description
 * # chessGameApp
 *
 * Main module of the application.
 */
angular
  .module('chessGameApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase'
  ])
  .value('fbURL', 'https://chess-angular.firebaseio.com/')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/board.html',
        controller: 'GameflowCtrl',
        controllerAs: 'gameflow'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
