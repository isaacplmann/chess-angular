'use strict';

/**
 * @ngdoc function
 * @name chessGameApp.controller:GameflowCtrl
 * @description
 * # GameflowCtrl
 * Controller of the chessGameApp
 */
angular.module('chessGameApp')
	.controller('GameflowCtrl', function ($scope, Match) {
		$scope.map = [
			[1,0,1,0,1,0,1,0],
			[0,1,0,1,0,1,0,1],
			[1,0,1,0,1,0,1,0],
			[0,1,0,1,0,1,0,1],
			[1,0,1,0,1,0,1,0],
			[0,1,0,1,0,1,0,1],
			[1,0,1,0,1,0,1,0],
			[0,1,0,1,0,1,0,1]
		];

		$scope.isValidMove = function(piece,newLocation) {
			if(newLocation.x < 0 || newLocation.x > 7 || newLocation.y<0 || newLocation.y>7) {
				// off the board
				return false;
			}
			for (var i = $scope.pieces.length - 1; i >= 0; i--) {
				var otherPiece = $scope.pieces[i];
				if(newLocation.x == otherPiece.location.x && newLocation.y == otherPiece.location.y) {
					// space occupied
					if(otherPiece.player==piece.player) {
						// can't land on your own pieces
						return false;
					} else {
						// attacking other player's piece
						return otherPiece;
					}
				}
			};
			return true;
		};
		$scope.highlightMove = function(piece,newLocation) {
			var result = $scope.isValidMove(piece,newLocation);
			if(result) {
				piece.possibleMoves.push(newLocation);

				if(typeof result === 'object') {
					newLocation.enemy = result;
					return false;
				} else {
					return true;
				}
			}
			return false;
		};
		$scope.highlightMoveIfEnemy = function(piece,newLocation) {
			var result = $scope.isValidMove(piece,newLocation);
			if(result && typeof result === 'object') {
				newLocation.enemy = result;
				piece.possibleMoves.push(newLocation);
			}
			return false;
		};

		$scope.movePiece = function(piece, location) {
			piece.location = location;
			$scope.pieces.$save(piece);
			$scope.nextTurn();
		};

		$scope.kill = function(piece) {
			piece.location = {x:-5,y:-5};
			piece.dead = true;
			$scope.pieces.$save(piece);
		};

		$scope.currentPiece;
		$scope.match = Match.match;
		$scope.match.$loaded(function() {
			if (!$scope.match.turn) {
				$scope.nextTurn();
			}
		});
		// $scope.turn = Match.match.turn;
		$scope.nextTurn = function() {
			if($scope.match.turn==1) {
				$scope.match.turn=2;
			} else {
				$scope.match.turn=1;
			}
			$scope.match.$save();
		};

		$scope.$on('showMoves',function(ev,piece){
			// var piece = $scope.pieces.find(function(piece) {return piece.$$hashKey==pieceHash});
			$scope.currentPiece = piece;
			piece.possibleMoves = [];
			// $scope.highlights = [];
			var direction = 1;
			if(piece.player==2) {
				direction = -1;
			}
			var moves = [];
			var x = piece.location.x;
			var y = piece.location.y;
			switch (piece.type) {
				case 'p':
					$scope.highlightMove(piece,{"x":x,"y":y+direction});
					if(piece.player==1 && y==1 ||
						piece.player==2 && y==6) {
						$scope.highlightMove(piece,{"x":x,"y":y+2*direction});
					}
					$scope.highlightMoveIfEnemy(piece,{"x":x+1,"y":y+direction});
					$scope.highlightMoveIfEnemy(piece,{"x":x-1,"y":y+direction});
					break;
				case 'R':
					var i=x+1;
					while($scope.highlightMove(piece,{"x":i,"y":y})) {
						i++;
					}
					i=x-1;
					while($scope.highlightMove(piece,{"x":i,"y":y})) {
						i--;
					}
					i=y+1;
					while($scope.highlightMove(piece,{"x":x,"y":i})) {
						i++;
					}
					i=y-1;
					while($scope.highlightMove(piece,{"x":x,"y":i})) {
						i--;
					}
					break;
				case 'k':
					$scope.highlightMove(piece,{"x":x-2,"y":y+1});
					$scope.highlightMove(piece,{"x":x-2,"y":y-1});
					$scope.highlightMove(piece,{"x":x-1,"y":y+2});
					$scope.highlightMove(piece,{"x":x-1,"y":y-2});
					$scope.highlightMove(piece,{"x":x+1,"y":y+2});
					$scope.highlightMove(piece,{"x":x+1,"y":y-2});
					$scope.highlightMove(piece,{"x":x+2,"y":y+1});
					$scope.highlightMove(piece,{"x":x+2,"y":y-1});
					break;
				case 'B':
					var i=x+1;
					var j=y+1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i++;
						j++;
					}
					i=x-1;
					j=y+1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i--;
						j++;
					}
					i=x+1;
					j=y-1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i++;
						j--;
					}
					i=x-1;
					j=y-1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i--;
						j--;
					}
					break;
				case 'K':
					$scope.highlightMove(piece,{"x":x-1,"y":y+1});
					$scope.highlightMove(piece,{"x":x-1,"y":y});
					$scope.highlightMove(piece,{"x":x-1,"y":y-1});
					$scope.highlightMove(piece,{"x":x,"y":y+1});
					$scope.highlightMove(piece,{"x":x,"y":y-1});
					$scope.highlightMove(piece,{"x":x+1,"y":y+1});
					$scope.highlightMove(piece,{"x":x+1,"y":y});
					$scope.highlightMove(piece,{"x":x+1,"y":y-1});
					break;
				case 'Q':
					var i=x+1;
					while($scope.highlightMove(piece,{"x":i,"y":y})) {
						i++;
					}
					i=x-1;
					while($scope.highlightMove(piece,{"x":i,"y":y})) {
						i--;
					}
					i=y+1;
					while($scope.highlightMove(piece,{"x":x,"y":i})) {
						i++;
					}
					i=y-1;
					while($scope.highlightMove(piece,{"x":x,"y":i})) {
						i--;
					}
					var i=x+1;
					var j=y+1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i++;
						j++;
					}
					i=x-1;
					j=y+1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i--;
						j++;
					}
					i=x+1;
					j=y-1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i++;
						j--;
					}
					i=x-1;
					j=y-1;
					while($scope.highlightMove(piece,{"x":i,"y":j})) {
						i--;
						j--;
					}
					break;
			}
		});

		$scope.pieces = Match.pieces;
		$scope.roomId = Match.roomId;
		$scope.pieces.$loaded(function() {
			if ($scope.pieces.length === 0) {
				for (var i = $scope.initialPieces.length - 1; i >= 0; i--) {
					var piece = $scope.initialPieces[i];
					$scope.pieces.$add(piece);
				};
			}
		});
	 	$scope.initialPieces = [
			{
				type:'R',
				player:1,
				location:{
					x:0,
					y:0
				}
			},
			{
				type:'k',
				player:1,
				location:{
					x:1,
					y:0
				}
			},
			{
				type:'B',
				player:1,
				location:{
					x:2,
					y:0
				}
			},
			{
				type:'K',
				player:1,
				location:{
					x:3,
					y:0
				}
			},
			{
				type:'Q',
				player:1,
				location:{
					x:4,
					y:0
				}
			},
			{
				type:'B',
				player:1,
				location:{
					x:5,
					y:0
				}
			},
			{
				type:'k',
				player:1,
				location:{
					x:6,
					y:0
				}
			},
			{
				type:'R',
				player:1,
				location:{
					x:7,
					y:0
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:0,
					y:1
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:1,
					y:1
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:2,
					y:1
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:3,
					y:1
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:4,
					y:1
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:5,
					y:1
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:6,
					y:1
				}
			},
			{
				type:'p',
				player:1,
				location:{
					x:7,
					y:1
				}
			},
			{
				type:'R',
				player:2,
				location:{
					x:0,
					y:7
				}
			},
			{
				type:'k',
				player:2,
				location:{
					x:1,
					y:7
				}
			},
			{
				type:'B',
				player:2,
				location:{
					x:2,
					y:7
				}
			},
			{
				type:'K',
				player:2,
				location:{
					x:3,
					y:7
				}
			},
			{
				type:'Q',
				player:2,
				location:{
					x:4,
					y:7
				}
			},
			{
				type:'B',
				player:2,
				location:{
					x:5,
					y:7
				}
			},
			{
				type:'k',
				player:2,
				location:{
					x:6,
					y:7
				}
			},
			{
				type:'R',
				player:2,
				location:{
					x:7,
					y:7
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:0,
					y:6
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:1,
					y:6
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:2,
					y:6
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:3,
					y:6
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:4,
					y:6
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:5,
					y:6
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:6,
					y:6
				}
			},
			{
				type:'p',
				player:2,
				location:{
					x:7,
					y:6
				}
			},
		]
	});
