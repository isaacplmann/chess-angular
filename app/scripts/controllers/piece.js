'use strict';

/**
 * @ngdoc function
 * @name chessGameApp.controller:PieceCtrl
 * @description
 * # PieceCtrl
 * Controller of the chessGameApp
 */
angular.module('chessGameApp')
	.controller('PieceCtrl', function ($scope, $attrs, $rootScope,$animate) {
		var self = this;
		var tileSize = 50;

		$scope.updateLocationStyle = function() {

			if($scope.piece.dead) {
				return {transform:'translateX('+tileSize*8+'px) translateY('+(($scope.piece.player-1)*tileSize*7)+'px)'};
			}
			return {
				transform:'translateX('+($scope.piece.location.x*tileSize)+'px) translateY('+($scope.piece.location.y*tileSize)+'px)',
			};
		};
		$scope.setLocationStyle = function() {
			$scope.locationStyle = $scope.updateLocationStyle();
		};

		$scope.locationStyle = $scope.updateLocationStyle();
		$scope.$watch("piece.location", $scope.setLocationStyle());

		$scope.showMoves = function() {
			$scope.piece.hide = false;
			$rootScope.$broadcast('showMoves',$scope.piece);
		};
		$scope.hideMoves = function() {
			$scope.piece.hide = true;
		};

	});
