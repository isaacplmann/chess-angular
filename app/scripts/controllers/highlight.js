'use strict';

/**
 * @ngdoc function
 * @name chessGameApp.controller:HighlightCtrl
 * @description
 * # HighlightCtrl
 * Controller of the chessGameApp
 */
angular.module('chessGameApp')
  .controller('HighlightCtrl', function ($scope) {
		$scope.updateLocationStyle = function(location) {
			if($scope.currentPiece && $scope.currentPiece.hide) {
				return {transform:'translateX('+(location.x*50)+'px) translateY('+(location.y*50)+'px) scale(0)'};
			}
			return {
				transform:'translateX('+(location.x*50)+'px) translateY('+(location.y*50)+'px) scale(1)',
			};
		};

		$scope.updateLocationStyles = function() {
			for (var i = 0; $scope.currentPiece && $scope.currentPiece.possibleMoves && i < $scope.currentPiece.possibleMoves.length; i++) {
				var location = $scope.currentPiece.possibleMoves[i];
				location.locationStyle = $scope.updateLocationStyle(location);
			};
		};
		$scope.$watch('currentPiece.possibleMoves', function() {
			$scope.updateLocationStyles();
		});
		$scope.$watch('currentPiece.hide', function() {
			$scope.updateLocationStyles();
		});

		$scope.movePiece = function(location) {
			// for (var i = 0; $scope.currentPiece && $scope.currentPiece.possibleMoves && i < $scope.currentPiece.possibleMoves.length; i++) {
			// 	var location = $scope.currentPiece.possibleMoves[i];
			// 	location = $scope.currentPiece.location;
			// };
			$scope.currentPiece.hide = true;
			$scope.updateLocationStyles();
			if(location.enemy && typeof location.enemy === 'object') {
				$scope.$parent.kill(location.enemy);
			}
			$scope.$parent.movePiece($scope.currentPiece,location);
			// $scope.currentPiece.location.x = location.x;
			// $scope.currentPiece.location.y = location.y;
			// $scope.currentPiece.type = "!";
		};
  });
