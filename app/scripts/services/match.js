'use strict';

/**
 * @ngdoc service
 * @name chessGameApp.Match
 * @description
 * # Match
 * Service in the chessGameApp.
 */
angular.module('chessGameApp')
  .factory('Match', function (fbURL, $firebaseArray, $firebaseObject, $location, $window) {
  	var roomId = $location.search().room;
  	if(!roomId) {
	  	roomId = Math.round(Math.random() * 100000000);
  	}
    $window.FirebasePieces = new $window.Firebase(fbURL+'games/'+roomId+'/pieces');
    $window.FirebaseMatch = new $window.Firebase(fbURL+'games/'+roomId+'/match');

    return {
    	pieces:$firebaseArray($window.FirebasePieces),
    	match:$firebaseObject($window.FirebaseMatch),
    	roomId: roomId
    };
  });
