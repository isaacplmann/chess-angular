'use strict';

/**
 * @ngdoc directive
 * @name chessGameApp.directive:highlight
 * @description
 * # highlight
 */
angular.module('chessGameApp')
  .directive('highlight', function () {
    return {
      template: '<div><a ng-repeat="move in currentPiece.possibleMoves" href class="piece highlight" ng-style="move.locationStyle" ng-click="movePiece(move)">{{currentPiece.type}}</a></div>',
      restrict: 'A',
      controller: 'HighlightCtrl',
      replace: true,
      link: function postLink(scope, element, attrs) {
      },
      scope: {
      	currentPiece:'='
      }
    };
  });
