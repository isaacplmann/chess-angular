'use strict';

/**
 * @ngdoc directive
 * @name chessGameApp.directive:tile
 * @description
 * # tile
 */
angular.module('chessGameApp')
  .directive('tile', function () {
    return {
      template: '<div class="tile{{tile}}"></div>',
      restrict: 'A',
      replace:true,
      link: function postLink(scope, element, attrs) {
      	// console.log(scope);
      }
    };
  });
