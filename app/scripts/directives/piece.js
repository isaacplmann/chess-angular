'use strict';

/**
 * @ngdoc directive
 * @name chessGameApp.directive:piece
 * @description
 * # piece
 */
angular.module('chessGameApp')
  .directive('piece', function () {
    return {
      template: '<button ng-disabled="$parent.match.turn==piece.player" href class="piece player{{piece.player}}" ng-style="locationStyle" ng-click="showMoves()" ng-blur="hideMoves()">{{piece.type}}</button>',
      controller: 'PieceCtrl',
      restrict: 'A',
      replace:true,
      link: function postLink(scope, element, attrs, pieceCtrl) {
      	pieceCtrl.init();
      	// console.log(scope);
      },
      scope:{
      	piece:'=',
      }
    };
  });
