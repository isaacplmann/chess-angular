// module.exports = function(config) {
//   config.set({
//     basePath: '../',
//     frameworks: ['mocha', 'chai'], //'browserify', , 'chai-things', 'sinon-chai', 'chai-as-promised'
//     plugins: [
//       "karma-phantomjs-launcher",
//       "karma-mocha",
//       "karma-mocha-reporter",
//       // 'karma-coverage',
//       // 'karma-requirejs',
//       'karma-chai',
//       'karma-chai-things',
//       'karma-sinon-chai',
//       'karma-chai-as-promised'
//     ],
//     files: [
//       "app/scripts/**/*.js",
//       // "test/mock/**/*.js",
//       "test/spec/**/*.js",
//       'node_modules/angular-mocks/angular-mocks.js'
//     ],
//     reporters: ['mocha'],
//     preprocessors: {
//       // 'spec/**/*.coffee': ['browserify']
//     },
//     // browserify: {
//     //   transform: ['coffeeify'],
//     //   extensions: ['.coffee']
//     // },
//     port: 9876,
//     colors: true,
//     logLevel: config.LOG_INFO,
//     autoWatch: false,
//     browserNoActivityTimeout: 60000,
//     browsers: ['PhantomJS_custom'],
//     customLaunchers: {
//       'PhantomJS_custom': {
//         base: 'PhantomJS',
//         options: {
//           settings: {
//             webSecurityEnabled: false
//           }
//         }
//       }
//     },
//     singleRun: true
//   });
// };

// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2015-07-13 using
// generator-karma 1.0.0

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    // as well as any additional frameworks (requirejs/chai/sinon/...)
    frameworks: [
      "mocha",
      // "requirejs",
      "chai"
      // "sinon-chai"
      // "chai-as-promised"
     ],

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'bower_components/jquery/dist/jquery.js',
      'bower_components/angular/angular.js',
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
      'bower_components/angular-animate/angular-animate.js',
      'bower_components/angular-cookies/angular-cookies.js',
      'bower_components/angular-messages/angular-messages.js',
      'bower_components/angular-resource/angular-resource.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-touch/angular-touch.js',
      'bower_components/firebase/firebase.js',
      'bower_components/angularfire/dist/angularfire.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/mocha/mocha.js',
      'bower_components/chai-as-promised/lib/chai-as-promised.js',
      'bower_components/mockfirebase/browser/mockfirebase.js',
      // endbower
      'node_modules/sinon/pkg/sinon.js',
      'node_modules/chai/chai.js',
      'node_modules/sinon-chai/lib/sinon-chai.js',
      "app/scripts/**/*.js",
      "test/mock/**/*.js",
      "test/spec/**/*.js"
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      "PhantomJS"
    ],

    // progress is the default reporter
    reporters: ['mocha','coverage'], //

    // map of preprocessors that is used mostly for plugins
    preprocessors: {
      'app/scripts/controllers/*.js': ['coverage'],
      'app/scripts/directives/*.js': ['coverage'],
      'app/scripts/app.js': ['coverage']
    },

    // Which plugins to enable
    plugins: [
      "karma-phantomjs-launcher",
      "karma-mocha",
      "karma-mocha-reporter",
      'karma-coverage',
      // 'karma-requirejs',
      'karma-chai',
      'sinon-chai'
      // 'karma-chai-as-promised'
    ],

    // add plugin settings
    coverageReporter: {
      // type of file to output, use text to output to console
      type : 'text',
      // directory where coverage results are saved
      dir: 'test-results/coverage/'
      // if type is text or text-summary, you can set the file name
      // file: 'coverage.txt'
    },

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
