'use strict';

describe('Controller: HighlightCtrl', function () {

  // load the controller's module
  beforeEach(module('chessGameApp'));

  var HighlightCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HighlightCtrl = $controller('HighlightCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    // expect(HighlightCtrl.awesomeThings.length).toBe(3);
  });
});
