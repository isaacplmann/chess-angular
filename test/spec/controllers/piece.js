'use strict';

// var helpers = require('../helpers/test-helper');
// var expect = helpers.expect;

describe('Controller: PieceCtrl', function () {

  // load the controller's module
  beforeEach(module('chessGameApp'));

  var PieceCtrl,
    scope; 

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    var self = this;

    self.attrs = sinon.stub();
    self.animate = sinon.stub();


    scope = $rootScope.$new();
    scope.piece = {
      hide:false,
      dead:false,
      location:{
        x:0,
        y:0
      }
    };
    PieceCtrl = $controller('PieceCtrl', {
      $scope: scope,
      $attrs: self.attrs,
      $animate: self.animate
    });
  }));

  it('should hide and show moves', function () {
    inject(function($rootScope) {
      var broadcastSpy = sinon.spy($rootScope,'$broadcast');
      expect(scope.piece.hide).to.equal(false);
      scope.hideMoves();
      expect(scope.piece.hide).to.equal(true);
      scope.showMoves();
      expect(scope.piece.hide).to.equal(false);
      broadcastSpy.should.have.been.calledWith('showMoves',scope.piece);
    });
  });

  it('should update location style when location changes', function () {
    var tileSize = 50;
    expect(scope.locationStyle.transform).to.equal('translateX('+(scope.piece.location.x*tileSize)+'px) translateY('+(scope.piece.location.y*tileSize)+'px)');
    scope.piece.location.x=1;
    scope.setLocationStyle();
    expect(scope.locationStyle.transform).to.equal('translateX('+(scope.piece.location.x*tileSize)+'px) translateY('+(scope.piece.location.y*tileSize)+'px)');

    scope.piece.dead = true;
    scope.setLocationStyle();
    expect(scope.locationStyle.transform).to.equal('translateX('+tileSize*8+'px) translateY('+((scope.piece.player-1)*tileSize*7)+'px)');
  });
});
