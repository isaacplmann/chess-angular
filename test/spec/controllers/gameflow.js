'use strict';

describe('Controller: GameflowCtrl', function () {

  // load the controller's module
  beforeEach(module('chessGameApp'));

  var GameflowCtrl,
    scope,
    match,
    firebasePieces,
    firebaseMatch;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope,Match,$window,fbURL) {
    scope = $rootScope.$new();

    GameflowCtrl = $controller('GameflowCtrl', {
      $scope: scope,
      Match: {
        match:{
          turn:1,
          $save:function() {},
          $loaded:function(callback) {
            callback();
          }
        },
        pieces:{
          $loaded:function(callback) {
            callback();
            scope.pieces = initialPieces;
            scope.pieces.$save = function() {};
          }
        },
        roomId:111
      }
    });

  }));

  it('should have default initial values', function () {
    expect(scope.currentPiece).to.be.undefined;
    expect(scope.pieces).to.have.length.above(2);
  });

  describe('isValidMove function', function() {
    it('should validate a valid move', function () {
      var knight = scope.pieces[1]; // top left knight
      var valid = scope.isValidMove(knight,{x:2,y:2});
      expect(valid).to.be.true;
    });
    it('should invalidate an invalid move', function () {
      var knight = scope.pieces[1]; // top left knight
      var invalidMoves = [
        {x:-1,y:0}, // off the board
        {x:1,y:-1}, // off the board
        {x:8,y:0}, // off the board
        {x:1,y:9}, // off the board
        {x:3,y:1} // friendly piece
      ];
      for (var i = 0; i < invalidMoves.length; i++) {
        expect(scope.isValidMove(knight,invalidMoves[i])).to.be.false;
      };
    });
    it('should return a captured piece object', function () {
      var knight = scope.pieces[1]; // top left knight
      var valid = scope.isValidMove(knight,{x:1,y:6});
      expect(valid).to.be.an('object');
    });
  });

  describe('highlightMove function', function() {
    it('should highlight a valid move', function () {
      var knight = scope.pieces[1]; // top left knight
      knight.possibleMoves = [];
      var valid = scope.highlightMove(knight,{x:2,y:2});
      expect(valid).to.be.true;
      expect(knight.possibleMoves.length).to.equal(1);
    });
    it('should not highlight an invalid move', function () {
      var knight = scope.pieces[1]; // top left knight
      knight.possibleMoves = [];
      expect(scope.highlightMove(knight,{x:-1,y:0})).to.be.false;
      expect(knight.possibleMoves.length).to.equal(0);
    });
    it('should flag a captured piece object', function () {
      var knight = scope.pieces[1]; // top left knight
      knight.possibleMoves = [];
      var valid = scope.highlightMove(knight,{x:1,y:6});
      expect(valid).to.be.false;
      expect(knight.possibleMoves.length).to.equal(1);
      expect(knight.possibleMoves[0].enemy).to.be.an('object');
    });
  });

  describe('highlightMoveIfEnemy function', function() {
    it('should not highlight a valid move', function () {
      var knight = scope.pieces[1]; // top left knight
      knight.possibleMoves = [];
      var valid = scope.highlightMoveIfEnemy(knight,{x:2,y:2});
      expect(valid).to.be.false;
      expect(knight.possibleMoves.length).to.equal(0);
    });
    it('should not highlight an invalid move', function () {
      var knight = scope.pieces[1]; // top left knight
      knight.possibleMoves = [];
      expect(scope.highlightMoveIfEnemy(knight,{x:-1,y:0})).to.be.false;
      expect(knight.possibleMoves.length).to.equal(0);
    });
    it('should flag a captured piece object', function () {
      var knight = scope.pieces[1]; // top left knight
      knight.possibleMoves = [];
      var valid = scope.highlightMoveIfEnemy(knight,{x:1,y:6});
      expect(valid).to.be.false;
      expect(knight.possibleMoves.length).to.equal(1);
      expect(knight.possibleMoves[0].enemy).to.be.an('object');
    });
  });

  describe('movePiece function', function() {
    it('should move the piece', function () {
      var saveMatchSpy = sinon.spy(scope.match,'$save');
      var savePieceSpy = sinon.spy(scope.pieces,'$save');
      var knight = scope.pieces[1]; // top left knight
      knight.possibleMoves = [];
      scope.movePiece(knight,{x:2,y:2});
      expect(knight.location.x).to.equal(2);
      expect(knight.location.y).to.equal(2);
      // savePieceSpy.should.have.been.called();
      expect(scope.match.turn).to.equal(2);
    });
  });

  describe('kill function', function() {
    it('should kill the piece', function () {
      var savePieceSpy = sinon.spy(scope.pieces,'$save');
      var knight = scope.pieces[1]; // top left knight
      scope.kill(knight);
      expect(knight.location.x).to.equal(-5);
      expect(knight.location.y).to.equal(-5);
      expect(knight.dead).to.be.true;
      // savePieceSpy.should.have.been.called();
    });
  });
});
